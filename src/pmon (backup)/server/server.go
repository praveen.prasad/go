package main

import(
	"fmt"
    "log"
	"time"
	"net/http"
	"github.com/gorilla/websocket"
	"encoding/json"
	"io/ioutil"
)

// We'll need to define an Upgrader
// this will require a Read and Write buffer size
var upgrader = websocket.Upgrader{
    ReadBufferSize:  1024,
    WriteBufferSize: 1024,
}



func homePage(w http.ResponseWriter, r *http.Request) {
    fmt.Fprintf(w, "Home Page")
}

func readJson() []byte {
		// read file
		data,err := ioutil.ReadFile("./sample.json")
		if err != nil {
			fmt.Print(err)
		}
	
		// switch struct
		type Switch struct{
			Name string `json:"name"`
			Value string `json:"value"`
		}
	
		// mainswitch struch
		type MainSwitch struct{
			MainSwitch []Switch `json:"mainswitch"`
		}
	
		// json data
		var mainswitch MainSwitch
	
		// unmarshall it
		err = json.Unmarshal(data, &mainswitch)
	
		if err != nil {
			fmt.Println("error:",err)
		}	
	
		// iterate json array
/*		for i := 0; i < len(mainswitch.MainSwitch); i++{
			fmt.Println("Switch Name :" + mainswitch.MainSwitch[i].Name)
			fmt.Println("Switch Value:" + mainswitch.MainSwitch[i].Value)
		}
*/

		bytes, err := json.Marshal(mainswitch)
		return bytes
		//var string p = string(data)
		//d := string(bytes)
		//fmt.Println(string(d))
		
}
// define a reader which will listen for
// new messages being sent to our WebSocket
// endpoint
func reader(conn *websocket.Conn) {
    for {
    // read in a message
    //    messageType, p, err := conn.ReadMessage()
    //    if err != nil {
    //        log.Println(err)
    //        return
    //   }
    // print out that message for clarity
	//    fmt.Println(string(p))
		time.Sleep(1 * time.Second)
		var p[] byte
		var messageType int
		messageType = 1
		p = readJson()

		//fmt.Println(string(p))
        if err := conn.WriteMessage(messageType, p); err != nil {
            log.Println(err)
            return
		}
	

    }
}

func wsEndpoint(w http.ResponseWriter, r *http.Request) {
    upgrader.CheckOrigin = func(r *http.Request) bool { return true }

    // upgrade this connection to a WebSocket
    // connection
    ws, err := upgrader.Upgrade(w, r, nil)
    if err != nil {
        log.Println(err)
    }
    fmt.Printf("Client Id:   %s\n", ws.RemoteAddr())
    
    // helpful log statement to show connections
    log.Println("Client Connected")
    reader(ws)
}


func setupRoutes() {
    http.HandleFunc("/", homePage)
    http.HandleFunc("/ws", wsEndpoint)
}

func startWebServer()  {
	fmt.Println("Starting Web Server")
    setupRoutes()
    log.Fatal(http.ListenAndServe(":8080", nil))

}

func main()  {
	
// start webserver
startWebServer();

//readJson();

}
