package main

import(
	"encoding/json"
	"fmt"
	"io/ioutil"
)

func main(){

	// read file
	data,err := ioutil.ReadFile("./sample.json")
	if err != nil {
		fmt.Print(err)
	}

	// switch struct
	type Switch struct{
		Name string `json:"name"`
		Value string `json:"value"`
	}

	// mainswitch struch
	type MainSwitch struct{
		MainSwitch []Switch `json:"mainswitch"`
	}

	// json data
	var mainswitch MainSwitch

	// unmarshall it
	err = json.Unmarshal(data, &mainswitch)

	if err != nil {
		fmt.Println("error:",err)
	}	

	// iterate json array
	for i := 0; i < len(mainswitch.MainSwitch); i++{
		fmt.Println("Switch Name :" + mainswitch.MainSwitch[i].Name)
		fmt.Println("Switch Value:" + mainswitch.MainSwitch[i].Value)
	}


}